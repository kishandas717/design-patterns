
# Design Patterns

## What is Design Pattern?

* Design Patterns are solutions to commonly occuring software problems.
* The 23 GOF (Gang of Four) patterns are considered the foundation for all other patterns.
* The GoF patterns are divided into three groups: Creational, Structural, and Behavioral.

## Types of Design Patterns

    1. Creational Design Patterns

        1. Abstract Factory
        2. Builder
        3. Factory Method
        4. Prototype
        5. Singleton

    2. Structural Design Patterns

        1. Adapter
        2. Bridge
        3. Composite
        4. Decorator
        5. Facade
        6. Flyweight
        7. Proxy

    3. Behavioral Design Patterns

        1. Chain of Resp.
        2. Command
        3. Interpreter
        4. Iterator
        5. Mediator
        6. Memento
        7. Observer
        8. State
        9. Strategy
        10. Template Method
        11. Visitor

## Abstract Factory

* An Abstract Factory creates objects related by a common theme.
* A Factory is an object that creates other objects, and Abstract Factory abstracts out the theme shared by newly created objects.
* Factories are used when constructor functions are limited in control over the overall creation process and broader knowledge is needed.

## Builder

* The Builder pattern simplifies client code that creates complex objects by hiding construction detaials.

## Factory Method

* Factory Method is a design pattern used to create new objects as instructed by client.
* It is used when the client does not know which of several candidate objects to instantiate.
* Factory Methods are used in applications that manage or manipulate colllections of objects with shared characteristics.

## Prototype

* The Prototype Pattern creates new objects initialised with values from a prototype object.
* It is useful for initialising business objects with values that match default values in the database.
* Classical languages don't often use the Prototype pattern, but Javascript does.

## Singleton

* The Singleton Pattern limits the number of instances of a particular object to just one, called the singleton.
* Singletons are useful for situations where system-wide actions need to be coordinated from a single central place, like database connection pool.
* Singletons reduce the need for global variables, reducing namespace pollution and name collision risks in Javascript.

## Adapter

* Adapter pattern translates one interface to another, allowing components with mismatched interface to work together.
* Commonly used when new components need to integrate with existing ones, or when old code still expects the original interface after refactoring.

## Bridge

* Bridge pattern enables two components, a client and a service, to work together with each component having its own interface.
* It has two levels of abstraction to write better code and facilitates loose coupling of objects.

## Composite

* The Composite pattern allows for the creation of objects with properties that are primitive items or a collection of objects.
* A tree control is a good example of the composite pattern in use.
* The composite pattern uses a common interface which simplifies the design and construction of recursive algorithms that iterate over each object in the collection.

## Decorator

* The Decorator pattern adds new behavior to an object dynamically.
* This is done through a Decorator object that wraps around the original object.
* Multiple decorators can add or override functionality to the original object.
* Decorators provide felxibity to stactically typed languages, but in Javascript the Extend and Mixin patterns are more relevent.

## Facade

* Facade pattern provides an interface that sheilds clients from complex functionality in subsystems.
* It is often present in systems with a multi-layer architecture.
* The Facade provides a high-level interface that makes a subsystems easy to use for the client.
* It is often used in a multi-layer web application for communication between the presentation and service layer, using a well-defined API.
* It can be used in refactoring to hide complicated legacy code behind a cleaner interface.
* Facades are frequently implemented as singleton factories and combined with other design patterns.

## Flyweight

* Flyweight is an object normalization technique that conserves memory by sharing fine-grained objects efficiently.
* Common properties are factored out into shared flyweight objects, making them immutable and shared with others.
* The Flyweight factory creates and manages flyweight objects and stores newly created flyweights for future requests.

## Proxy

* The Proxy pattern creates a placeholder object for another object and controls access to the original object.
* It is used when an object is constrained and cannot perform its responsibilty efficiently.
* The Proxy forwards requests to the target object and has the same interface as the original object.

## Chain of Resp.

* The Chain of Responsibility pattern involves loosely coupled objects that can handle a request.
* It is essentially a linear search for an object that can handle a particular request.
* It is related to the Chaining Pattern used in JavaScript and jQuery.
* The pattern involves a Client who initiates the request and a Handler who defines an interface for handling requests.

## Command

* The Command pattern separates the objects that issue a request from the objects that process requests, which are called events and event handlers.
* Command objects can centralize the processing of actions and are involved in handling Undo functionality. 

## Interpreter

* The Interpreter pattern allows for a scripting language that lets end-users customize their solution.
* Applications that require advanced configuration can benefit from offering a basic scripting language for users to manipulate.
* The pattern maps a language to a grammar, which is a hierarchical tree-like structure that ends up with literals.
* Using the Interpreter design pattern, problems expressed in this grammar can be implemented as a language.

## Iterator

* The Iterator pattern allows clients to loop over a collection of objects.
* It separates the collection from the traversal by implementing a specialized iterator.
* JavaScript supports basic looping but the Iterator pattern allows for more flexibility.

## Mediator

* The Mediator pattern provides central authority over a group of objects by encapsulating how these objects interact.
* This pattern is useful for managing complex conditions in which every object is aware of any state change in any other object in the group.
* One example of the Mediator pattern is coordinating arrivals and departures of airplanes in an airport control tower.

## Memento

* The Memento pattern provides temporary storage and restoration of an object.
* It stores an object’s state in a small repository called a Memento.
* This pattern is used to easily undo subsequent changes to an object's state.
* In JavaScript, Mementos are easily implemented using JSON.

## Observer

* The Observer pattern is a subscription model used in event-driven programming, including JavaScript.
*  It allows objects to subscribe to an event and get notified when it occurs.
* The pattern promotes loose coupling and facilitates good object-oriented design.
* The Observer pattern is also known as Pub/Sub.
* The participants in the pattern are the Subject and Observers.
* The Subject maintains a list of observers, lets them subscribe or unsubscribe, and sends notifications when its state changes.
* The Observers have a function signature that is invoked when the Subject changes.

## State

* The State pattern provides state-specific logic to a limited set of objects.
* This pattern is best explained by the example of a customer placing an online order for a TV.
* The order can be in one of many states: New, Approved, Packed, Pending, Hold, Shipping, Completed, or Canceled.
* The State pattern can be applied to this scenario by providing 8 State objects, each with its own set of properties and methods for acceptable state transitions.

## Strategy

* The Strategy pattern allows for interchangeable algorithms for a task.
* In JavaScript, it is commonly used as a plug-in mechanism for extensible frameworks.
* The pattern requires that all method signatures be the same for interchangeable use.

## Template

* The Template Method pattern provides an outline of steps for an algorithm.
* Objects that implement these steps can redefine or adjust certain steps while retaining the original structure of the algorithm, offering extensibility to client developers.
* Template Methods are frequently used in general purpose frameworks or libraries.
* Example: an object that fires a sequence of events in response to an action, such as a process request, with the option for developers to adjust the response.

## Visitor

* The Visitor pattern allows for a new operation to be applied to a collection of objects without altering the objects themselves.
* The new operation is carried out by a separate object called the Visitor.
* Visitors are useful for building extensibility into libraries or frameworks.
* In most programming languages, the Visitor pattern requires anticipating future functional adjustments and including methods that accept a Visitor object.
* However, in JavaScript, the flexibility of being able to add and remove methods at runtime eliminates the need for this anticipation.
var TrafficLight = function() {
    var count = 0;
    var currentState = new Red(this);

    this.change = function(state) {
        if (count++ >= 10) return;
        currentState = state;
        currentState.go(); 
    }

    this.start = function() {
        currentState.go();
    }
}

var Red = function(light) {
    this.light = light;

    this.go = function() {
        console.log("Red --> for 1 minute");
        light.change(new Green(light));
    }
}

var Green = function(light) {
    this.light = light;

    this.go = function() {
        console.log("Green --> for 1 minute");
        light.change(new Yellow(light));
    }
}

var Yellow = function(light) {
    this.light = light;

    this.go = function() {
        console.log("Yellow --> for 10 secons");
        light.change(new Red(light));
    }
}

function run() {
    var light = new TrafficLight();
    light.start();
}

run();